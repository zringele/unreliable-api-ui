<?php

return [
    'base_url' => env('UNRELIABLE_API_ENDPOINT', 'localhost:56665'),
    'data_endpoint' => env('UNRELIABLE_API_DATA', '/api/data/'),
    'last_update_endpoint' => env('UNRELIABLE_API_LAST_TIMESTAMP', '/api/last-update'),
    'interesting_fields' => [
        ['name' => 'customer_billing_minutes_called_to_macau', 'type' => ['decimal', 8, 3]],
        ['name' => 'customer_has_outgoing_activity_si804', 'type' => ['decimal', 8, 3]],
        ['name' => 'cata_package_revenue_app_ev_gestores_moneylender', 'type' => ['decimal', 8, 3]],
        ['name' => 'customer_core_market', 'type' => ['decimal', 8, 3]],
        ['name' => 'customer_billing_minutes_called_to_nicaragua', 'type' => ['decimal', 8, 3]],
    ],
    'table_name' => 'user_data',
    'previous_table_name' => 'user_data_backup'
];

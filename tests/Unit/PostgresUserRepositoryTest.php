<?php

namespace Tests\Unit;

use Tests\TestCase;
use Faker\Provider\Base;
use App\Model\Repository\User\PostgresUserRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class PostgresUserRepositoryTest extends TestCase
{
	protected $userRepository;

	public function setUp(): void
	{
		$this->userRepository = new PostgresUserRepository();
		parent::setUp();
	}

	public function testFilterIsParsedCorrectly()
	{
		$filter = 'eq:500';
		[$operator, $operand] = $this->userRepository->getFilterDetails($filter);
		$this->assertEquals('=', $operator);
		$this->assertEquals('500', $operand);
	}

	public function testExceptionIsThrownWithBadFilter()
	{
		$filter = 'eq500';
		$this->expectException(BadRequestException::class);
		$this->userRepository->getFilterDetails($filter);
	}

	public function testImportInitializationCreatesTmpTable()
    {
    	$this->userRepository->setUpImport();
		$this->assertDatabaseCount('users_tmp', 0);
    }

	public function testUpdateDataInsertsOneRow()
	{
		$data = $this->generateRandomData();
		$this->userRepository->setUpImport();
		$this->userRepository->updateData([$data]);
		$this->assertDatabaseCount('users_tmp', 1);
	}

	protected function generateRandomData()
	{
		$columns = config('unreliable_api.interesting_fields');
		$data = [];
		foreach ($columns as $name => $column) {
			$fakeFloat = Base::randomFloat(3,-900,900);
			$data[$column['name']] = $fakeFloat;
		}
		$data['id'] = Base::randomNumber(2);
		$data['lastUpdated'] = Base::randomNumber(9, true);
		return $data;
	}
}

<?php

namespace App\Providers;

use App\Facades\UnreliableApi;
use App\Facades\UnreliableApiFetcher\UnreliableApiFetcher;
use App\Model\Repository\User\PostgresUserRepository;
use App\Model\Repository\User\UserRepositoryContract;
use App\Services\UnreliableApiImporter\UnreliableApiImporter;
use App\Services\UnreliableApiImporter\UnreliableApiImporterContract;
use Illuminate\Support\ServiceProvider;

class UnreliableApiServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UnreliableApiImporterContract::class, UnreliableApiImporter::class);
        $this->app->bind('unreliable-api', UnreliableApiFetcher::class);
        $this->app->bind(UserRepositoryContract::class, PostgresUserRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Model\Repository\User\UserRepositoryContract;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function search(Request $request)
    {
        $filteredUsers = $this->userRepository->filterFromRequest($request);
        return $filteredUsers;
    }

    public function columns()
    {
        $columns = $this->userRepository->interestingColumns();
        return $columns;
    }
}

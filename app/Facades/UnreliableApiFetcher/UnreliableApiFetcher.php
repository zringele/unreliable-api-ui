<?php
declare(strict_types=1);

namespace App\Facades\UnreliableApiFetcher;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class UnreliableApiFetcher implements UnreliableApiFetcherContract
{
	protected $fetchEndpoint;

	protected $lastUpdatedEndpoint;

	protected $currentPage;

	protected $lastKnownTimestamp;

	protected $totalPages;

	protected $perPage;

	public function __construct()
	{
		$baseUrl = config('unreliable_api.base_url');
		$this->fetchEndpoint = $baseUrl . config('unreliable_api.data_endpoint');
		$this->lastUpdatedEndpoint = $baseUrl . config('unreliable_api.last_update_endpoint');
	}

	public function getCurrentPage(): int
	{
		return $this->currentPage ?? 0;
	}

	public function isLastPage(): bool
	{
		return $this->getCurrentPage() >= $this->totalPages - 1;
	}

	public function getTotalPages(): int
	{
		return $this->totalPages ?? 0;
	}

	/**
	 * Not currently used
	 *
	 * @return array
	 */
	public function fetchAll(): array
	{
		return [];
	}

	public function fetch(int $page): array
	{
		$decodedBody = $this->getData($page);
		return $decodedBody['data'];
	}

	/**
	 * Fetches current page and sets next page
	 *
	 * @param bool $moveToNext
	 * @return array
	 */
	public function fetchCurrent(bool $moveToNext = true): array
	{
		$decodedBody = $this->getData($this->getCurrentPage());
		$this->currentPage = $this->getCurrentPage() + 1;
		$this->perPage = $decodedBody['perPage'];
		$this->totalPages = $decodedBody['pages'];

		return $decodedBody['data'];
	}

	public function getLastKnownUpdate(): int
	{
		return $this->lastKnownTimestamp ?? 0;
	}

	public function getLastUpdate(): int
	{
		$responseBody = Http::get($this->lastUpdatedEndpoint)->body();
		return (int)$responseBody;
	}

	protected function getData(int $page): array
	{
		// We keep trying to get data until we succeed, maybe add time or attempts limit?
		while (true) {
			try {

				$start = microtime(true);
				$responseBody = Http::get($this->fetchEndpoint . $page)->body();
				$end = microtime(true);

				//This takes up to 2seconds sometimes. Is it really possible to have fresh data within 2sec?
				Log::info("Microseconds to fetch page " . ($end - $start));

				$start = microtime(true);
				$decodedBody = json_decode($responseBody, true);
				$end = microtime(true);

				Log::info("Microseconds to decode page " . ($end - $start));

				//Throw some exceptions if request is wrong in some ways
				if (isset($decodedBody['error'])) {
					throw new FailedFetch('Error message found');
				}
				if (!isset($decodedBody['data'])) {
					throw new FailedFetch('No data found');
				}
				if (!isset($decodedBody['perPage'])) {
					throw new FailedFetch('No perPage found');
				}
				if (!isset($decodedBody['pages'])) {
					throw new FailedFetch('No pages found');
				}
			} catch (FailedFetch $e) {
				continue;
			}
//			  If we run into memory problems, could splice array
//            $decodedBody['data'] = array_splice($decodedBody['data'], (int) $decodedBody['perPage']);

			return $decodedBody;
		}
		return [];
	}

	/**
	 * Get first page and set page to 1
	 *
	 * @return array
	 */
	public function fetchFirstPage(): array
	{
		$decodedBody = $this->getData(0);
		$this->perPage = $decodedBody['perPage'];
		$this->totalPages = $decodedBody['pages'];
		$this->currentPage = 1;
		return $decodedBody['data'];
	}

}

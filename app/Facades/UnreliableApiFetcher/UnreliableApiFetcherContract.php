<?php
declare(strict_types=1);


namespace App\Facades\UnreliableApiFetcher;


interface UnreliableApiFetcherContract
{
    public function getCurrentPage(): int;

    public function isLastPage(): bool;

    public function fetchAll(): array;

    public function getTotalPages(): int;

    public function getLastKnownUpdate(): int;

    public function fetch(int $page): array;

    public function fetchCurrent(bool $moveToNext): array;
}

<?php
declare(strict_types=1);

namespace App\Services\UnreliableApiImporter;

interface UnreliableApiImporterContract
{
	public function updateData(): int;
}

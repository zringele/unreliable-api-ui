<?php
declare(strict_types=1);

namespace App\Services\UnreliableApiImporter;

use App\Facades\UnreliableApi;
use App\Model\Repository\User\UserRepositoryContract;
use Symfony\Component\Process\Process;

class UnreliableApiImporter implements UnreliableApiImporterContract
{
	protected $userRepository;
	/**
	 * Collection of Processes
	 * @var Process[]
	 */
	protected $processes = [];

	public function __construct(UserRepositoryContract $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	public function updateData(): int
	{
		// Get first page for pagination
		$firstPage = UnreliableApi::fetchFirstPage();

		$this->userRepository->setUpImport();

		// Start async importing fir every page on separate process
		for ($i = 1; $i < UnreliableApi::getTotalPages(); $i++) {
			$this->importPage($i);
		}
		$this->userRepository->updateData($firstPage);

		// Wait for processes to finish
		while (true) {
			if (empty($this->processes)) {
				break;
			}

			foreach ($this->processes as $key => $process) {
				if ($process->getOutput()) {
					echo $process->getOutput();
				}

				if (!$process->isRunning()) {
					unset($this->processes[$key]);
				}
				$process->clearOutput();
			}

			usleep(2000);
		}

		$this->userRepository->finishImport();

		return UnreliableApi::getLastKnownUpdate();
	}

	public function importPage($page): void
	{
		$process = Process::fromShellCommandline("cd \"" . app()->basePath() . "\" && php artisan import:page $page");

		$process->start();

		$this->processes[] = $process;
	}
}

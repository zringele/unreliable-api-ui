<?php
declare(strict_types=1);

namespace App\Model\Repository\User;

use App\Model\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class PostgresUserRepository implements UserRepositoryContract
{

	protected $operators = [
		'lte' => '<=',
		'gte' => '>=',
		'eq'  => '=',
	];

	public function filterFromRequest(Request $request)
	{
		$requestParams = $request->all();

		$columnFilters = array_intersect_key($requestParams, $this->getFillableKeys());

		$order = $requestParams['order'] ?? 'id';

		$direction = $requestParams['direction'] ?? 'desc';

		$query = User::with('notes')->orderBy($order, $direction);

		$page = $requestParams['page'] ?? 1;

		$perPage = $requestParams['per_page'] ?? 20;

		$this->addColumnFiltersToQuery($query, $columnFilters);

		return $query->paginate($perPage, $this->interestingColumns(), 'page', (int)$page);
	}

	public function setUpImport(): void
	{
		$this->createTempTable();
	}

	protected function addColumnFiltersToQuery($query, $columnFilters)
	{
		foreach ($columnFilters as $key => $filter) {
			[$operator, $operand] = $this->getFilterDetails($filter);
			$query->where($key, $operator, $operand);
		}
	}

	public function getFilterDetails($filter)
	{
		$exploded = explode(':', $filter);

		if (count($exploded) !== 2) {
			throw new BadRequestException('Incorrect filter ' . $filter);
		}

		$operator = $this->operators[$exploded[0]];

		return [$operator, $exploded[1]];
	}

	/**
	 * @param array $data
	 * @return bool
	 */
	public function updateData(array $data): bool
	{
		// Keep only interesting columns
		$fillableKeys = $this->getFillableKeys();
		foreach ($data as &$datum) {
			$datum['last_updated'] = $datum['lastUpdated'];
			$datum = array_intersect_key($datum, $fillableKeys);
		}

		DB::table('users_tmp')->insertOrIgnore($data);

		return true;
	}

	/**
	 * Get array of interesting columns as keys
	 *
	 * @return array
	 */
	protected function getFillableKeys()
	{
		return array_flip($this->interestingColumns());
	}

	/**
	 * We consider interesting columns here as every column.
	 * @return array
	 */
	public function interestingColumns(): array
	{
		return Schema::getColumnListing('users_tmp');
	}

	/**
	 * Create users_tmp table based on config
	 */
	protected function createTempTable(): void
	{
		Schema::dropIfExists('users_tmp');
		Schema::create(
			'users_tmp', function(Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('last_updated');
			foreach (config('unreliable_api.interesting_fields') as $interstingColumn) {
				$type = $interstingColumn['type'][0];
				unset($interstingColumn['type'][0]);
				$table->{$type}($interstingColumn['name'], ...$interstingColumn['type'])->nullable();
			}
		}
		);
	}

	/**
	 * Renames tables once importing to tmp table is done
	 */
	public function finishImport(): void
	{
		Schema::dropIfExists('users_backup');
		if (Schema::hasTable('users')) {
			Schema::rename('users', 'users_backup');
			$this->dropBackupIndexes();
		}
		Schema::rename('users_tmp', 'users');
	}

	protected function dropBackupIndexes()
	{
		//If we need indexes, should drop from backup table when finished
	}
}

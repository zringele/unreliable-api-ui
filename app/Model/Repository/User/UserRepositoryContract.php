<?php
declare(strict_types=1);

namespace App\Model\Repository\User;

use Illuminate\Http\Request;

interface UserRepositoryContract
{
	public function updateData(array $data): bool;

	public function setUpImport(): void;

	public function finishImport(): void;

	public function filterFromRequest(Request $request);
}

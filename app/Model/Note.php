<?php
declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
	protected $fillable = ['user_id', 'note'];
}

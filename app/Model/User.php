<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	use ColumnFillable;

	/**
	 * @return mixed
	 */
	public function notes()
	{
		return $this->hasMany(Note::class);
	}
}

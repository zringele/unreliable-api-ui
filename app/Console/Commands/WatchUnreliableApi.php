<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Facades\UnreliableApi;
use App\Services\UnreliableApiImporter\UnreliableApiImporterContract;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class WatchUnreliableApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'watch:unreliable_api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Watch for changes in unreliable api and updates tables';

    /**
     * Unreliable ai importer instance
     *
     * @var UnreliableApiImporterContract
     */
    protected $importer;

    /**
     * WatchUnreliableApi constructor.
     * @param UnreliableApiImporterContract $importer
     */
    public function __construct(UnreliableApiImporterContract $importer)
    {
        $this->importer = $importer;

        parent::__construct();
    }

    /**
     * Keep updating users from unreliable api
     *
     */
    public function handle()
    {
        $lastUpdate = 0;
        while(true) {
            if ($lastUpdate < UnreliableApi::getLastUpdate()) {
                $start =  microtime(true);
                $this->importer->updateData();
                $end =  microtime(true);
                dump($end - $start);
            } else {
                usleep(10000);
            }
        }

    }
}

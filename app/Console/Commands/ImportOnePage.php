<?php

namespace App\Console\Commands;

use App\Facades\UnreliableApi;
use App\Model\Repository\User\UserRepositoryContract;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ImportOnePage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:page {page}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $userRepository;

    /**
     * ImportOnePage constructor.
     * @param UserRepositoryContract $userRepository
     */
    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
        parent::__construct();
    }


    public function handle()
    {
        $data = UnreliableApi::fetch((int)$this->argument('page'));
        $this->userRepository->updateData($data);


    }
}

# Reliable API

### Project setup
```
composer install
```
```
php artisan migrate
```

Create `.env` file and set up environment

### Running
```
php artisan watch:unreliable_api
```
```
php artisan serve
```